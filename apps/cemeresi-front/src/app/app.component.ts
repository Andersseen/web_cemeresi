import { Component } from '@angular/core';
import { ClarityIcons, userIcon } from '@cds/core/icon';
import { bounceInAnimation } from 'angular-animations';

ClarityIcons.addIcons(userIcon);

@Component({
  selector: 'web-cemeresi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    bounceInAnimation({
      anchor: 'bounceIcon',
      duration: 500,
    }),
  ],
})
export class AppComponent {
  clickIcon = false;
  title = 'Clinica Cemeresi';

  menuIconState = false;
  chatIconState = false;

  animate(state: string) {
    switch (state) {
      case 'menu':
        this.menuIconState = false;
        setTimeout(() => {
          this.menuIconState = true;
        }, 1);
        break;
      case 'chat':
        this.chatIconState = false;
        setTimeout(() => {
          this.chatIconState = true;
        }, 1);
        break;
      default:
        this.menuIconState = false;
        this.chatIconState = false;
        break;
    }
  }
}
