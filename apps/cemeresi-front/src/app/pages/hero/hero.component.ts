import { Component } from '@angular/core';
import {
  zoomInOnEnterAnimation,
  zoomInDownOnEnterAnimation,
} from 'angular-animations';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
  animations: [
    zoomInOnEnterAnimation({
      anchor: 'enter1',
      duration: 500,
    }),
    zoomInDownOnEnterAnimation({
      anchor: 'enter2',
      duration: 700,
      delay: 200,
    }),
  ],
})
export class HeroComponent {}
