import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'hero',
    loadChildren: () =>
      import('./pages/hero/hero.module').then((m) => m.HeroModule),
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('./pages/contact/contact.module').then((m) => m.ContactModule),
  },
  {
    path: '',
    redirectTo: 'hero',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'hero',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
