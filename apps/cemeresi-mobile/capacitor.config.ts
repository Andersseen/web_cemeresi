import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'cemeresi-mobile',
  webDir: '../../dist/apps/cemeresi-mobile',
  bundledWebRuntime: false,
};

export default config;
