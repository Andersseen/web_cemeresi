import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Patient } from '../../../assets/data/interfaces';
import { PATIENTS } from '../../../assets/data/mock-customers';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'web-cemeresi-new-patien',
  templateUrl: './new-patient.page.html',
  styleUrls: ['./new-patient.page.scss'],
})
export class NewPatientPage implements OnInit {
  public newPatientForm!: FormGroup;
  public patient: Patient | null = null;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.newPatientForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: [''],
      birthday: [''],
      email: [''],
      phone: [''],
      note: [''],
      date: [''],
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        const patient =
          this.router.getCurrentNavigation()?.extras.state?.['patient'];

        this.newPatientForm.controls['name'].setValue(patient.name);
        this.newPatientForm.controls['lastName'].setValue(patient.lastName);
        this.newPatientForm.controls['gender'].setValue(patient.gender);
        this.newPatientForm.controls['birthday'].setValue(patient.birthday);
        this.newPatientForm.controls['email'].setValue(patient.email);
        this.newPatientForm.controls['phone'].setValue(patient.phone);
        this.newPatientForm.controls['note'].setValue(patient.note);
        this.newPatientForm.controls['date'].setValue(patient.date);
        this.patient = patient;
      }
    });
  }

  onClickResetForm() {
    this.newPatientForm.reset();
  }
  onClickCreatePatient() {
    const patient = new Patient();
    patient.id = `${PATIENTS.length + 1}`;
    patient.name = this.newPatientForm.controls['name'].value;
    patient.lastName = this.newPatientForm.controls['lastName'].value;
    patient.gender = this.newPatientForm.controls['gender'].value;
    patient.birthday = this.newPatientForm.controls['birthday'].value;
    patient.email = this.newPatientForm.controls['email'].value;
    patient.phone = this.newPatientForm.controls['phone'].value;
    patient.note = this.newPatientForm.controls['note'].value;
    patient.date = this.newPatientForm.controls['date'].value;
    this.patient = patient;

    console.log(this.patient);
    PATIENTS.push(this.patient);
  }

  goBack() {
    this.router.navigate(['/patients']);
  }
  backToCreatePatient() {
    this.onClickResetForm();
    this.patient = null;
  }
}
