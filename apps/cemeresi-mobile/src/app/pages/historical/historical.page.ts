import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Patient } from '../../../assets/data/interfaces';

@Component({
  selector: 'web-cemeresi-historical',
  templateUrl: './historical.page.html',
  styleUrls: ['./historical.page.scss'],
})
export class HistoricalPage implements OnInit {
  public title = 'Historical';
  public patient: Patient | null = null;
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        const patient =
          this.router.getCurrentNavigation()?.extras.state?.['patient'];
        this.patient = patient;
      }
    });
  }
  onClickAddHistorial() {
    console.log('added historical');
  }

  goBack() {
    this.router.navigate(['/patients']);
  }
}
