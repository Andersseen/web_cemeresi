import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { HistoricalPage } from './historical.page';

const routes: Routes = [
  {
    path: '',
    component: HistoricalPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [HistoricalPage],
  exports: [RouterModule],
})
export class HistoricalModule {}
