import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'web-cemeresi-hero',
  templateUrl: './hero.page.html',
  styleUrls: ['./hero.page.scss'],
})
export class HeroPage implements OnInit {
  public toolbarTitle = 'Clinica Cemeresi';
  public contentTitle = 'PARA NOSOTROS LO MÁS IMPORTANTE ERES TU';
  public videoPath = '../assets/video/web_Alberto.webm';
  constructor() {}

  ngOnInit() {}
}
