import { Component, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';
import { CustomerRow } from '../../../assets/data/interfaces';
import { Patient } from '../../../assets/data/interfaces';
import { PatientsService } from '../../services/patients.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'web-cemeresi-patients',
  templateUrl: './patients.page.html',
  styleUrls: ['./patients.page.scss'],
})
export class PatientsPage implements OnInit {
  @ViewChild(IonModal) modal!: IonModal;
  idBool = true;
  nameBool = true;
  lastNameBool = true;
  genderBool = true;
  birthdayBool = false;
  phoneBool = false;
  emailBool = false;
  noteBool = false;
  dateBool = false;
  checkeds = 4;
  limit = 4;
  podecheck = true;
  rowsChecked: Array<CustomerRow> = [
    { name: 'Id', checked: true },
    { name: 'Nomber', checked: true },
    { name: 'Apellido', checked: true },
    { name: 'Gender', checked: true },
  ];

  customers?: Patient[];
  aaa = false;
  customerRows: CustomerRow[] = [
    { name: 'Id', checked: true },
    { name: 'Nomber', checked: true },
    { name: 'Apellido', checked: true },
    { name: 'Gender', checked: true },
    { name: 'Cumpleaños', checked: false },
    { name: 'Telefono', checked: false },
    { name: 'Email', checked: false },
    { name: 'Notas', checked: false },
    { name: 'Fecha', checked: false },
  ];
  constructor(
    private actionSheetCtrl: ActionSheetController,
    private patientsService: PatientsService,
    private router: Router
  ) {}
  patients: Patient[] = [];

  ngOnInit() {
    // fetch('../../../assets/data/customers.json')
    //   .then((res) => res.json())
    //   .then((json) => {
    //     this.customers = json;
    //   });

    this.getPatients();
  }

  getPatients(): void {
    this.patientsService
      .getPatients()
      .subscribe((patient: Patient[]) => (this.patients = patient));
  }

  cancel() {
    this.modal.dismiss(null, 'cancel');
  }
  confirm() {
    this.modal.dismiss(this.customerRows, 'confirm');
  }
  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<CustomerRow[]>>;
    if (ev.detail.role === 'confirm') {
      this.rowsChecked = [];
      const filterRows = ev.detail.data;
      filterRows?.forEach((element: CustomerRow) => {
        if (element.checked === true) {
          this.rowsChecked.push(element);
        }
      });
      this.idBool = false;
      this.nameBool = false;
      this.lastNameBool = false;
      this.genderBool = false;
      this.birthdayBool = false;
      this.phoneBool = false;
      this.emailBool = false;
      this.noteBool = false;
      this.dateBool = false;
      this.rowsChecked.forEach((el) => {
        switch (el.name) {
          case 'Id':
            this.idBool = true;
            break;
          case 'Nomber':
            this.nameBool = true;
            break;
          case 'Apellido':
            this.lastNameBool = true;
            break;
          case 'Gender':
            this.genderBool = true;
            break;
          case 'Cumpleaños':
            this.birthdayBool = true;
            break;
          case 'Telefono':
            this.phoneBool = true;
            break;
          case 'Email':
            this.emailBool = true;
            break;
          case 'Notas':
            this.noteBool = true;
            break;
          case 'Fecha':
            this.dateBool = true;
            break;
          default:
            console.log('aaa');
        }
      });
    }
  }

  check(entry: CustomerRow) {
    if (!entry.checked) {
      this.checkeds++;
    } else {
      this.checkeds--;
    }
  }

  async presentActionSheet(patient: Patient) {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Elije la accion con este paciente',
      subHeader: `${patient.name} ${patient.lastName}`,
      buttons: [
        {
          text: 'Historico',

          data: {
            action: 'historical',
          },
        },
        {
          text: 'Editar',
          data: {
            action: 'edit',
          },
        },
        {
          text: 'Eliminar',
          role: 'destructive',
          data: {
            action: 'delete',
          },
        },
        {
          text: 'Volver',
          role: 'cancel',
          data: {
            action: 'cancel',
          },
        },
      ],
    });

    await actionSheet.present();
    const result = await actionSheet.onDidDismiss();
    if (result.data.action === 'cancel') {
      return;
    }
    const navigationExtras: NavigationExtras = {
      state: {
        patient: patient,
      },
    };
    switch (result.data.action) {
      case 'historical':
        this.router.navigate(['/historical'], navigationExtras);
        break;
      case 'edit':
        this.router.navigate(['/newpatient'], navigationExtras);

        break;
      case 'delete':
        break;
      default:
        break;
    }
  }
}
