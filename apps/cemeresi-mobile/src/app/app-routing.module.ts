import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'hero',
    pathMatch: 'full',
  },

  {
    path: 'hero',
    loadChildren: () =>
      import('./pages/hero/hero.module').then((m) => m.HeroModule),
  },
  {
    path: 'newpatient',
    loadChildren: () =>
      import('./pages/new-patient/new-patient.module').then(
        (m) => m.NewPatientModule
      ),
  },
  {
    path: 'patients',
    loadChildren: () =>
      import('./pages/patients/patients.module').then((m) => m.PatientsModule),
  },
  {
    path: 'historical',
    loadChildren: () =>
      import('./pages/historical/historical.module').then(
        (m) => m.HistoricalModule
      ),
  },
  {
    path: '**',
    redirectTo: 'hero',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
