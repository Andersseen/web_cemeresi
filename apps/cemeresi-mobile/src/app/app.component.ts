import { Component } from '@angular/core';

@Component({
  selector: 'web-cemeresi-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inicio', url: '/hero', icon: 'albums' },
    { title: 'Crear paciente', url: '/newpatient', icon: 'add-circle' },
    {
      title: 'Pacientes',
      url: '/patients',
      icon: 'happy',
    },
  ];

  constructor() {}
}
