import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Patient } from '../../assets/data/interfaces';
import { PATIENTS } from '../../assets/data/mock-customers';

@Injectable({
  providedIn: 'root',
})
export class PatientsService {
  constructor() {}

  getPatients(): Observable<Patient[]> {
    const heroes = of(PATIENTS);
    return heroes;
  }
}
