export class CustomerRow {
  name!: string;
  checked!: boolean;
}
export class Patient {
  id!: string;
  name!: string;
  lastName!: string;
  gender!: string;
  birthday!: string;
  phone!: string;
  email!: string;
  note!: string;
  date!: string;
}
