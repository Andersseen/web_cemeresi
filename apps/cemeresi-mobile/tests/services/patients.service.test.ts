import { TestBed } from '@angular/core/testing';
import { Patient } from 'apps/cemeresi-mobile/src/assets/data/interfaces';
import { PatientsService } from '../../src/app/services/patients.service';

describe('PatientsService', () => {
  let service: PatientsService;
  const testPatient = {
    id: '1',
    name: 'Teresa',
    lastName: 'Abascal Morte',
    gender: 'Mujer',
    birthday: '01/02/1990',
    phone: '123456789',
    email: 'dasdsad@asdsqd.com',
    note: 'algo',
    date: '21/10/2021',
  };
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientsService);
  });

  test('should be created', async () => {
    expect(service).toBeTruthy();
  });

  test('should return expected patients', (done) => {
    service.getPatients().subscribe((patient: Patient[]) => {
      expect(patient.length).toBe(3);
      expect(patient[0]).toEqual(testPatient);
      done();
    });
  });
});
