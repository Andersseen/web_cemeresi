import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { AppComponent } from '../src/app/app.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { routes } from '../src/app/app-routing.module';

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let compiled: HTMLElement;
  let location: Location;
  let router: Router;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [IonicModule, RouterTestingModule.withRoutes(routes)],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    app = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
    fixture.ngZone?.run(() => {
      router.initialNavigation();
    });
  }));

  test('should create the app', () => {
    expect(app).toBeTruthy();
  });

  test('should have menu labels', () => {
    const menuItems = compiled.querySelectorAll('ion-label');
    expect(menuItems.length).toEqual(3);
    expect(menuItems[0].textContent).toContain('Inicio');
    expect(menuItems[1].textContent).toContain('Crear paciente');
    expect(menuItems[2].textContent).toContain('Pacientes');
  });

  test('should have urls', () => {
    const menuItems = compiled.querySelectorAll('ion-item');
    expect(menuItems.length).toEqual(3);
    expect(menuItems[0].getAttribute('ng-reflect-router-link')).toEqual(
      '/hero'
    );
    expect(menuItems[1].getAttribute('ng-reflect-router-link')).toEqual(
      '/newpatient'
    );
    expect(menuItems[2].getAttribute('ng-reflect-router-link')).toEqual(
      '/patients'
    );
  });
  test('should navigate to /hero', fakeAsync(() => {
    fixture.ngZone?.run(() => {
      router.initialNavigation();
      router.navigate(['']);
      tick();
      expect(location.path()).toBe('/hero');
    });
  }));

  test('should navigate to /newpatient', fakeAsync(() => {
    fixture.ngZone?.run(() => {
      router.initialNavigation();
      router.navigate(['newpatient']);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(location.path()).toBe('/newpatient');
      });
    });
  }));
  test('should navigate to /patients', waitForAsync(() => {
    fixture.ngZone?.run(() => {
      fixture.whenStable().then(() => {
        router.navigate(['/patients']).then(() => {
          expect(location.path()).toEqual('/patients');
        });
      });
    });
  }));
});
