import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { PatientsPage } from '../../../src/app/pages/patients/patients.page';

describe('Empty NewPatientPage', () => {
  let component: PatientsPage;
  let fixture: ComponentFixture<PatientsPage>;
  let compiled: HTMLElement;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PatientsPage],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();
    fixture = TestBed.createComponent(PatientsPage);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });
});
