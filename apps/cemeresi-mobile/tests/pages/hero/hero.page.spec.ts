import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HeroPage } from '../../../src/app/pages/hero/hero.page';

describe('HeroPage', () => {
  let component: HeroPage;
  let fixture: ComponentFixture<HeroPage>;
  let compiled: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HeroPage],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(HeroPage);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test("toolbarTitle should to be equal 'Clinica Cemeresi' ", () => {
    expect(component.toolbarTitle).toEqual('Clinica Cemeresi');
  });

  test("contentTitle should to be equal 'PARA NOSOTROS LO MÁS IMPORTANTE ERES TU'' ", () => {
    expect(component.contentTitle).toEqual(
      'PARA NOSOTROS LO MÁS IMPORTANTE ERES TU'
    );
  });
  test("videoPath should to be equal '../assets/video/web_Alberto.webm' ", () => {
    expect(component.videoPath).toEqual('../assets/video/web_Alberto.webm');
  });

  test('video should work', () => {
    const videoTag = compiled.querySelector('#video');
    expect(videoTag?.isConnected).toBeTruthy();
  });
});
