import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { HistoricalPage } from '../../../src/app/pages/historical/historical.page';

describe('HistoricalPage', () => {
  let component: HistoricalPage;
  let fixture: ComponentFixture<HistoricalPage>;
  let compiled: HTMLElement;
  const patient = {
    id: '1',
    name: 'Teresa',
    lastName: 'Abascal Morte',
    gender: 'Mujer',
    birthday: '01/02/1990',
    phone: '123456789',
    email: 'dasdsad@asdsqd.com',
    note: 'algo',
    date: '21/10/2021',
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HistoricalPage],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();
    fixture = TestBed.createComponent(HistoricalPage);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  }));
  test('should create', () => {
    expect(component).toBeTruthy();
  });
  test('should have the  page title', () => {
    expect(component.title).toBe('Historical');
    const title = compiled.querySelector('#pageTitle');
    expect(title?.textContent).toBe(component.title);
  });
  test("shouldn't have a  name title", () => {
    const title = compiled.querySelector('#nameTitle');
    expect(title).toBeFalsy;
  });

  test('should have textarea', () => {
    const textarea = compiled.querySelector('ion-textarea');
    expect(textarea?.isConnected).toBeTruthy;
    expect(textarea?.textContent).toContain('');
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    textarea!.value = 'someValue';
    textarea?.dispatchEvent(new Event('input'));
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(textarea!.value).toBe('someValue');
  });
  test('should have three buttons', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons.length).toBe(3);
  });
  test('should have button go back', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[0].textContent).toBe('Volver ');
  });
  test('should have button create', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[1].textContent).toBe('Crear ');
  });
  test('should have button reset', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[2].textContent).toBe('Resetear ');
  });

  test('should write name title', () => {
    component.patient = patient;
    fixture.detectChanges();
    const title = compiled.querySelector('#nameTitle');
    expect(title?.textContent).toBe('Teresa Abascal Morte');
  });
});
