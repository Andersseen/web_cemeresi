import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { NewPatientPage } from '../../../src/app/pages/new-patient/new-patient.page';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Navigation, Router, UrlTree } from '@angular/router';

describe('Empty NewPatientPage', () => {
  let component: NewPatientPage;
  let fixture: ComponentFixture<NewPatientPage>;
  let compiled: HTMLElement;
  const testPatient = {
    id: '4',
    name: 'test',
    lastName: 'testtest',
    gender: 'test',
    birthday: '01/02/2000',
    phone: '123456789',
    email: 'test@test.com',
    note: 'something',
    date: '01/02/2022',
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NewPatientPage],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(NewPatientPage);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });
  test('form invalid when empty', () => {
    expect(component.newPatientForm.valid).toBeFalsy();
  });

  test('name field invalid when empty', () => {
    const name = component.newPatientForm.controls['name'];
    expect(name.valid).toBeFalsy();
  });

  test('name field validity', () => {
    let errors: any = {};
    const name = component.newPatientForm.controls['name'];
    errors = name.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  test('email field valid when empty', () => {
    const email = component.newPatientForm.controls['email'];
    expect(email.valid).toBeTruthy();
  });

  test('email field validity', () => {
    let errors: any = {};
    const email = component.newPatientForm.controls['email'];
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
  });

  test("should have three buttons if doesn't exist patient", () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons.length).toBe(3);
  });
  test('should have button go back', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[0].textContent).toBe('Volver ');
  });
  test('should have button create', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[1].textContent).toBe('Crear ');
  });
  test('should have button reset', () => {
    const buttons = compiled.querySelectorAll('ion-button');
    expect(buttons[2].textContent).toBe('Resetear ');
  });
  test('patient should create from form submit', fakeAsync(() => {
    const acceptBtn = compiled.querySelector('#createBtn');

    component.newPatientForm.controls['name'].setValue(testPatient.name);
    component.newPatientForm.controls['lastName'].setValue(
      testPatient.lastName
    );
    component.newPatientForm.controls['gender'].setValue(testPatient.gender);
    component.newPatientForm.controls['birthday'].setValue(
      testPatient.birthday
    );
    component.newPatientForm.controls['phone'].setValue(testPatient.phone);
    component.newPatientForm.controls['email'].setValue(testPatient.email);
    component.newPatientForm.controls['note'].setValue(testPatient.note);
    component.newPatientForm.controls['date'].setValue(testPatient.date);
    expect(component.newPatientForm.valid).toBeTruthy();
    acceptBtn?.dispatchEvent(new Event('click'));
    expect(component.patient).toEqual(testPatient);
  }));
});

// TESTING WITH PATIENT

describe('NewPatientPage with patient', () => {
  let component: NewPatientPage;
  let fixture: ComponentFixture<NewPatientPage>;
  let compiled: HTMLElement;
  let router: Router;
  const testPatient = {
    id: '4',
    name: 'test',
    lastName: 'testtest',
    gender: 'test',
    birthday: '01/02/2000',
    phone: '123456789',
    email: 'test@test.com',
    note: 'something',
    date: '01/02/2022',
  };
  const mockNavigation: Navigation = {
    extras: {
      state: {
        patient: testPatient,
      },
    },
    id: 0,
    initialUrl: new UrlTree(),
    extractedUrl: new UrlTree(),
    trigger: 'imperative',
    previousNavigation: null,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NewPatientPage],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
    }).compileComponents();
    router = TestBed.inject(Router);
    jest.spyOn(router, 'getCurrentNavigation').mockReturnValue(mockNavigation);
    fixture = TestBed.createComponent(NewPatientPage);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should have four buttons if patient exist', () => {
    const buttons = compiled.querySelectorAll('ion-button');

    expect(buttons.length).toBe(4);
  });

  test('patient should update from form changes', fakeAsync(() => {
    expect(component.newPatientForm.controls['name'].value).toBe(
      testPatient.name
    );
    expect(component.newPatientForm.controls['lastName'].value).toBe(
      testPatient.lastName
    );
    expect(component.newPatientForm.controls['gender'].value).toBe(
      testPatient.gender
    );
    expect(component.newPatientForm.controls['birthday'].value).toBe(
      testPatient.birthday
    );
    expect(component.newPatientForm.controls['phone'].value).toBe(
      testPatient.phone
    );
    expect(component.newPatientForm.controls['email'].value).toBe(
      testPatient.email
    );
    expect(component.newPatientForm.controls['note'].value).toBe(
      testPatient.note
    );
    expect(component.newPatientForm.controls['date'].value).toBe(
      testPatient.date
    );

    expect(component.newPatientForm.valid).toBeTruthy();
  }));
});
